SI Mailer
=========

SI Mailer is a simple Python Script that allows a batch of emails with merge tags
to be sent to a group of recipients using your own SMTP Server. This can be useful
when you cannot utilize a dedicated SMTP Server (like Sendgrid) due to domain
restrictions and want to maintain maximum deliverability.

It is important to be aware however that most SMTP server providers, including
dedicated services, limit the number of emails that you can send both per hour
and per day. It is important to be aware of these limits when sending messages as
exceeding the limits can get your account blocked in some cases, like with Gmail.

## Using the Script
Once you have Python 2 installed on your computer (if you are using a Mac or most
linux operating systems, you are in luck since it is already installed), all you
need to do is run the following line in your terminal.

```
python processBatch.py -m <Path to Message HTML> -r <Path to Recipients CSV>
```

You will need to know the paths to your merge file and HTML template file, which
is easily achieved on most terminal simulators by simply dragging and dropping
the desired file into the desired terminal session window.

On first run, you will be prompted for your SMTP Server credentials, as well as
information about the Sender of the emails. You will be prompted for a Send Delay,
which is based on your SMTP servers sending limitations. For non-dedicated systems,
we recommend a value between 2500 and 10000 (milliseconds) between sends.

If you ever need top update the configuration, simply run the following command
similar to how you ran the initial merge command.
```
python processBatch.py --update
```

## Example
A sample template(`sample_template.html`) and merge file (`sample_merge_list.csv`)
are included in this repository to allow you to try out the script without having
to create your own merge list or template. **Be sure to update the email address(es)
in the merge file to your own.** If you do not update the email address, the sent
message will not reach you and will be bounced (this could negatively impact your
sender reputation).
