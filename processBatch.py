#! /usr/bin/python
# Written By Tom Paulus, @tompaulus, www.tompaulus.com

import csv
import getopt
import json
import logging
import os
import smtplib
import sys
import time
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from os.path import expanduser

logging.basicConfig(level=logging.WARN, format='%(asctime)s - %(levelname)s - %(message)s')


class Config(object):
    config_directory = expanduser("~") + ("/" if not expanduser("~").endswith("/") else '') + '.si'
    config_json_path = config_directory + '/config.json'
    config_dict = {'smtp': dict(),
                   'email': dict()}

    smtp_user = None
    smtp_password = None
    smtp_server = None
    smtp_port = None

    email_from_name = None
    email_from_address = None
    email_send_delay = None

    @classmethod
    def load_config(cls):
        """
        Load the Config JSON from the File in the User's Home Dir

        :return: If config file was loaded correctly
         :rtype: bool
        """
        logging.debug("Config Path is: " + cls.config_json_path)

        try:
            with open(cls.config_json_path) as config_file:
                cls.config_dict = json.loads(config_file.read())
        except IOError:
            logging.warn('Config File not found!')
            return False

        try:
            cls.load_properties_from_dict()
        except KeyError:
            logging.error("Malformed Config File")
            return False

        return True

    @classmethod
    def load_properties_from_dict(cls):
        """
        Set the Class variables based on the values in the config dictionary.

        :exception KeyError: Thrown if a Key is not in the Dict
        """
        cls.smtp_user = cls.config_dict['smtp']['user']
        cls.smtp_password = cls.config_dict['smtp']['password']
        cls.smtp_server = cls.config_dict['smtp']['server']
        cls.smtp_port = cls.config_dict['smtp']['port']

        cls.email_from_name = cls.config_dict['email']['from_name']
        cls.email_from_address = cls.config_dict['email']['from_address']
        cls.email_send_delay = cls.config_dict['email']['send_delay']

    @classmethod
    def save_config(cls):
        """
        Save the File in the User's Home Dir.
        """
        logging.debug("Config Path is: " + cls.config_json_path)

        if not os.path.exists(cls.config_directory):
            os.makedirs(cls.config_directory)

        with open(cls.config_json_path, 'w') as config_file:
            config_json = json.dumps(cls.config_dict)
            logging.debug("Config JSON: " + config_json)
            config_file.write(config_json)

    @classmethod
    def make_config(cls):
        """
        Make the Config Dictionary by polling the user for the corresponding fields.
        Be sure to call .save_config() after this function
        """

        print "Let's Setup the Server and Sender Settings. You will be able to confirm your entries at the end."
        # Prompt For Input

        smtp_server_input = raw_input(
            "SMTP Server%s: " % (' [' + cls.smtp_server + ']' if cls.smtp_server is not None else '')).strip()
        if smtp_server_input != '':
            cls.config_dict['smtp']['server'] = smtp_server_input

        smtp_port_input = raw_input(
            "SMTP Port%s: " % (' [' + str(cls.smtp_port) + ']' if cls.smtp_port is not None else '')).strip()
        if smtp_port_input != '':
            cls.config_dict['smtp']['port'] = int(smtp_port_input)

        smtp_username_input = raw_input(
            "SMTP Username%s: " % (' [' + cls.smtp_user + ']' if cls.smtp_user is not None else '')).strip()
        if smtp_username_input != '':
            cls.config_dict['smtp']['user'] = smtp_username_input

        smtp_password_input = raw_input(
            "SMTP Password%s: " % (' [' + cls.smtp_password + ']' if cls.smtp_password is not None else '')).strip()
        if smtp_password_input != '':
            cls.config_dict['smtp']['password'] = smtp_password_input

        email_address_input = raw_input(
            "From Address%s: " % (
                ' [' + cls.email_from_address + ']' if cls.email_from_address is not None else '')).strip()
        if email_address_input != '':
            cls.config_dict['email']['from_address'] = email_address_input

        email_name_input = raw_input(
            "From Name%s: " % (' [' + cls.email_from_name + ']' if cls.email_from_name is not None else '')).strip()
        if email_name_input != '':
            cls.config_dict['email']['from_name'] = email_name_input

        email_delay_input = raw_input(
            "Send Delay (millis)%s: " % (
                ' [' + str(cls.email_send_delay) + ']' if cls.email_send_delay is not None else '')).strip()
        if email_delay_input != '':
            cls.config_dict['email']['send_delay'] = long(email_delay_input)

        # Confirm Input
        print "SMTP Server: {}\n" \
              "SMTP Port: {}\n" \
              "SMTP Username: {}\n" \
              "SMTP Password: {}\n" \
              "From Address: {}\n" \
              "From Name: {}\n" \
              "Send Delay: {}".format(cls.config_dict['smtp']['server'],
                                      cls.config_dict['smtp']['port'],
                                      cls.config_dict['smtp']['user'],
                                      cls.config_dict['smtp']['password'],
                                      cls.config_dict['email']['from_address'],
                                      cls.config_dict['email']['from_name'],
                                      cls.config_dict['email']['send_delay'])
        conf = raw_input("Is this correct? [Y/n]")
        if conf.lower() == "y" or conf.lower() == "":
            cls.load_properties_from_dict()
        else:
            cls.make_config()


class ParseCSV(object):
    users = list()
    user_fields = list()

    @classmethod
    def load_csv(cls, path):
        """
        Load and Parse the CSV File with recipients and Merge Fields
        The first column must have their email address, and the second must have their Name.
        The Name will be what in the TO field.

        :param path: CSV Path
         :type path: str
        """
        header = True
        with open(path) as csv_file:
            reader = csv.reader(csv_file)
            for row in reader:
                if header:
                    cls.user_fields = row[2:]
                    header = False
                    logging.info("Merge Fields: " + str(cls.user_fields))
                    continue

                user = dict()
                user['email'] = row[0].lower()
                user['name'] = row[1]
                logging.debug("Found User: %s<%s>" % (user['email'], user['name']))

                for f in range(len(cls.user_fields)):
                    user[cls.user_fields[f].replace('{{', '').replace('}}', '')] = row[f + 2]

                cls.users.append(user)


class Email(object):
    message_template = ''

    def __init__(self):
        self.smtpserver = smtplib.SMTP(Config.smtp_server, Config.smtp_port)
        self.subject = ''
        self.html_message = Email.message_template

    def compose(self, user):
        """
        Replace the merge tags with the User's Values

        :param user: Message Recipient
         :type user: dict
        """
        for merge_tag in user.keys():
            merge_value = user[merge_tag]
            self.html_message = self.html_message.replace('{{%s}}' % merge_tag, merge_value)

    def send(self, to):
        """
        Send the Message to the Recipient

        :param to: Format "Recipient Name <email@address.tdl>"
        :type to: str
        :return: Message Send Status
         :rtype: bool
        """
        try:
            msgRoot = MIMEMultipart('related')
            msgRoot['Subject'] = self.subject
            msgRoot['From'] = "%s <%s>" % (Config.email_from_name, Config.email_from_address)
            msgRoot['To'] = to
            msgRoot['Date'] = formatdate(localtime=True)

            msgAlternative = MIMEMultipart('alternative')
            msgRoot.attach(msgAlternative)

            msgText = MIMEText(self.html_message, 'html', _charset='utf-8')
            msgAlternative.attach(msgText)

            self.smtpserver.ehlo()
            self.smtpserver.starttls()
            self.smtpserver.ehlo()
            self.smtpserver.login(Config.smtp_user, Config.smtp_password)
            self.smtpserver.sendmail(Config.smtp_user, [to], msgRoot.as_string())
            self.smtpserver.quit()
            return True

        except smtplib.SMTPException, e:
            logging.exception("Problem Sending Message", e)
            return False


def process_args(argv):
    update = False
    html = None
    csv = None

    try:
        opts, args = getopt.getopt(argv, "huom:r:", ["help", "update", "message=", "recipients="])
    except getopt.GetoptError:
        print 'processBatch.py -m <HTML Message File> -r <CSV Recipients File>'
        sys.exit(2)

    for opt, arg in opts:
        if opt in ('-h', "--help"):
            print 'processBatch.py -m <HTML Message File> -r <CSV Recipients File>'
            sys.exit()
        elif opt in ("-u", "--update"):
            update = True
            logging.info('User requested to update config info')
        elif opt in ("-m", "--message"):
            html = arg
        elif opt in ("-r", "--recipients"):
            csv = arg

    if not update:
        logging.debug("HTML Path: " + html)
        logging.debug("CSV Path: " + csv)

    return update, html, csv


if __name__ == '__main__':
    updated_requested, html_path, csv_path = process_args(sys.argv[1:])

    if not Config.load_config() or updated_requested:
        Config.make_config()
        Config.save_config()

        if updated_requested:
            sys.exit(0)

    ParseCSV.load_csv(csv_path)
    with open(html_path) as html_message_file:
        Email.message_template = html_message_file.read()

    message_subject = None
    while True:
        message_subject = raw_input("What is the Subject for your Message?%s " % (
            ' [' + message_subject + ']' if message_subject is not None else '')).strip()

        print "Subject: " + message_subject
        conf = raw_input("Is this correct? [Y/n]")
        if conf.lower() == "y" or conf.lower() == "":
            break

    last_message_sent = 0

    for u in ParseCSV.users:
        email = Email()
        email.subject = message_subject
        email.compose(u)
        user_email = "%s <%s>" % (u['name'], u['email'])

        delta = time.time() - last_message_sent
        if delta < Config.email_send_delay:
            logging.debug("Delaying %d seconds, due to configured sending delay" % delta)
            time.sleep(Config.email_send_delay - delta / 1000)

        logging.warn("Sending Message to: " + user_email)
        status = email.send(user_email)
        if not status:
            logging.error("Problem sending message to " + user_email)
        else:
            logging.debug("Message Sent!")
